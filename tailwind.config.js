/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      keyframes: {
        wiggle: {
          "0%": { transform: "scale(1.05, 1.05)" },
          // "50%": { transform: "scale(1.05, 1.05)" },
          "100%": { transform: "scale(1.05, 1.05)" },
        },
      },
      animation: {
        wiggle: "wiggle 1s ease infinite",
      },
    },
  },
  plugins: [],
};
