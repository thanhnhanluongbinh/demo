import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Product from "./views/Product";
import { BrowserRouter } from "react-router-dom";
import { Navigate, Route, Routes } from "react-router";
import Detail from "./views/Detail";
import ProductThunk from "./views/ProductThunk";

function App() {
  return (
    <div>
      {/* <Product/> */}

      <BrowserRouter>
        <Routes>
          <Route path="/redux" element={<Product />} />
          <Route path="/thunk" element={<ProductThunk />} />
          <Route path="/detail/:id" element={<Detail />} />
          <Route path="/" element={<Navigate to={"/thunk"} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
