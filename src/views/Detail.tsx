import React, { Dispatch, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { Action, Store } from "redux";
import { ThunkDispatch } from "redux-thunk";
import CardDetail from "../components/CardDetail";
import {
  DetailActionType,
  ProductActionType,
  ProductResult,
} from "../productsReduxThunk/models/ActionsTypes";
import { DetailType } from "../productsReduxThunk/models/DetailType";
import { AppDispatch, AppState } from "../productsReduxThunk/ProductThunkStore";
import {
  detailAction,
  getProductDetail,
  getProductList,
} from "../productsReduxThunk/ProductThunkStore/actions/ProductAction";
import { getProductDetailAPI } from "../productsReduxThunk/services/ProductServices";
export type ParamsType = ReturnType<typeof useParams>;
export const useAppDispatch = () => useDispatch<AppDispatch>();
function Detail() {
  const { id } = useParams();
  // const { detail } = useSelector((state: AppState) => state.productReducer);
  const [detail, setDetail] = useState<DetailType>();
  const getDetail = async (product: string) => {
    const data = await getProductDetailAPI(product);
    setDetail(data);
  };
  console.log("detail", detail);
  useEffect(() => {
    getDetail(id === undefined ? "" : id);
  }, []);
  return (
    <div className="max-w-5xl  m-auto py-4">
      {detail && (
        <CardDetail
          id={id === undefined ? "" : id}
          images={detail.media.images[0].url}
          name={detail.name}
          des={detail.description}
          price={detail.price.current.text}
        />
      )}
    </div>
  );
}

export default Detail;
