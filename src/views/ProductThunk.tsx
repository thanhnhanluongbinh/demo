import React, { Component } from "react";
import { connect } from "react-redux";
import Card from "../components/Card";
import { productType } from "../productsReduxThunk/models/ProductType";
import { AppState } from "../productsReduxThunk/ProductThunkStore";
import {
  getProductDetail,
  getProductList,
} from "../productsReduxThunk/ProductThunkStore/actions/ProductAction";
import { NavLink } from "react-router-dom";
import { ProductResult } from "../productsReduxThunk/models/ActionsTypes";

interface MyProps {
  productReducer: ProductResult;
  dispatch: Function;
}

export class ProductThunk extends React.Component<MyProps, {}> {
  handleClick = (idPd: string) => {
    alert("Add " + idPd);
  };

  componentDidMount(): void {
    this.props.dispatch(getProductList());
  }
  render() {
    //  console.log(this.props.productList);

    console.log(this.props);
    // const { productList, dispatch } = this.props;
    // console.log("productList: ", productList);
    const {
      productReducer: { productList },
    } = this.props;
    return (
      <div className="max-w-7xl grid grid-cols-4 m-auto">
        {productList.map((pd, index) => (
          <NavLink to={`/detail/${pd.id}`}>
            <Card
              key={index}
              id={pd.id}
              image={"https://" + pd.imageUrl}
              name={pd.name}
              price={pd.price.current.text}
              btn="Add"
              handleClick={this.handleClick}
            />
          </NavLink>
        ))}
      </div>
    );
  }
}

const mapStatetoProps = (state: AppState) => ({ ...state });

export default connect(mapStatetoProps)(ProductThunk);
