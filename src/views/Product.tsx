import React, { Component } from "react";
import { connect } from "react-redux";
import Card from "../components/Card";
import { productType } from "../productsRedux/models/ProductType";
import { AppState } from "../productsRedux/ProductStore";
import { NavLink } from "react-router-dom";

interface MyProps {
  productList: Array<productType>;
  dispatch: Function;
}

export class Product extends React.Component<MyProps, {}> {
  handleClick = (title: string) => {
    alert("bạn chọn " + title);
  };

  componentDidMount(): void {}
  render() {
    //  console.log(this.props.productList);

    console.log(this.props);
    const { productList, dispatch } = this.props;
    console.log("productList: ", productList);

    return (
      <div className="max-w-7xl grid grid-cols-4 m-auto">
        {productList.map((pd, index) => (
          <NavLink to={`/detail/${pd.id}`}>
            <Card
              key={index}
              id={pd.id}
              image={pd.imageUrl}
              name={pd.name}
              price={pd.price.current.text}
              btn="Add"
              handleClick={this.handleClick}
            />
          </NavLink>
        ))}
      </div>
    );
  }
}

const mapStatetoProps = (state: AppState) => ({ ...state.productReducer });

export default connect(mapStatetoProps)(Product);
