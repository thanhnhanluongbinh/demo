import React from "react";
interface MyProps {
  id: string;
  name: string;
  price: string;
  btn: string;
  image: string;
  handleClick: (id: string) => void;
}
const Card: React.FC<MyProps> = ({
  id,
  name,
  price,
  btn,
  image,
  handleClick,
}) => {
  return (
    <div className=" m-4 p-3">
      <div className="relative  hover:animate-wiggle cursor-pointer ">
        <img className="rounded-md w-full  " src={image} alt="" />
        <div className="bg-[#0000005c] absolute top-0 left-0 w-full h-full rounded-md" />
        <div className="absolute bottom-0 p-3 w-full h-1/3 ">
          <div className="flex justify-between items-center ">
            <div className="p-3">
              <p className="h-12 overflow-ellipsis overflow overflow-hidden text-[#b8df13] font-bold text-[17px]">
                {name}
              </p>

              <p className="text-[#df1313] font-bold text-[16px]">{price}</p>
            </div>
            <button
              className="rounded-lg bg-blue-600 p-2 hover:bg-blue-800"
              onClick={() => handleClick(id)}
            >
              {btn}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
