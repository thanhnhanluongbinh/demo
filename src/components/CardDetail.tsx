import React from "react";
import { ParamsType } from "../views/Detail";

interface AppProps {
  images: string;
  id: string;
  name: string;
  price: string;
  des: string;
}

const CardDetail: React.FC<AppProps> = (props) => {
  const { images, id, name, price, des } = props;
  console.log("images: ", images);

  return (
    <div className="flex ">
      <img src={"https://" + images} alt="" />
      <div className="mx-7">
        <h2 className="text-4xl text-[#1200ff]">{name}</h2>
        <div
          className="mx-2 text-[#126d6d] "
          dangerouslySetInnerHTML={{ __html: des }}
        ></div>
        <p className="m-2 text-red-500 text-xl">{price}</p>
      </div>
    </div>
  );
};
export default CardDetail;
