import { combineReducers, createStore, compose, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import * as reducers from "./reducers";
import reduxThunk from "redux-thunk";
const rootReducers = combineReducers({
  ...reducers,
});
export type AppState = ReturnType<typeof rootReducers>;
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  rootReducers,
  composeWithDevTools(applyMiddleware(reduxThunk))
);
