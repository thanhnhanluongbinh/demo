import { productType } from "../../models/ProductType";

export interface stateDefault {
  productList: Array<productType>;
}
interface Action {
  type: string;
  payload: Object;
}
const initialState: stateDefault = {
  productList: [
    {
      id: "1",
      name: "abc",
      price: {
        current: {
          text: "6000",
        },
      },
      imageUrl:
        "https://images.asos-media.com/products/vans-old-skool-canvas-sneakers-in-triple-white/202542334-1-white",
    },
    {
      id: "2",
      name: "def",
      price: {
        current: { text: "7000" },
      },
      imageUrl:
        "https://images.asos-media.com/products/vans-old-skool-canvas-sneakers-in-triple-white/202542334-1-white",
    },
    {
      id: "3",
      name: "ghd",
      price: {
        current: { text: "8000 " },
      },
      imageUrl:
        "https://images.asos-media.com/products/vans-old-skool-canvas-sneakers-in-triple-white/202542334-1-white",
    },

    {
      id: "5",
      name: "ink",
      price: {
        current: { text: "11000" },
      },
      imageUrl:
        "https://images.asos-media.com/products/vans-old-skool-canvas-sneakers-in-triple-white/202542334-1-white",
    },
    {
      id: "6",
      name: "red",
      price: {
        current: { text: "4000" },
      },
      imageUrl:
        "https://images.asos-media.com/products/vans-old-skool-canvas-sneakers-in-triple-white/202542334-1-white",
    },
    {
      id: "7",
      name: "auio",
      price: {
        current: { text: "2000" },
      },
      imageUrl:
        "https://images.asos-media.com/products/vans-old-skool-canvas-sneakers-in-triple-white/202542334-1-white",
    },
  ],
};

export const productReducer = (
  state: stateDefault = initialState,
  action: Action
) => {
  switch (action.type) {
    default:
      return state;
  }
};
