import {
  GET_PRODUCT_FAILED,
  GET_PRODUCT_REQUEST,
  GET_PRODUCT_SUCCESS,
} from "../ProductStore/constants/constants";
import { productType } from "./ProductType";

interface ProductAsync {
  loading: boolean;
  productList: productType[];
  err: string;
}

interface getProductSuccess extends ProductAsync {
  type: typeof GET_PRODUCT_SUCCESS;
}
interface getProductFailed extends ProductAsync {
  type: typeof GET_PRODUCT_FAILED;
}
interface getProductRequest extends ProductAsync {
  type: typeof GET_PRODUCT_REQUEST;
}

export type ProductActionType =
  | getProductSuccess
  | getProductRequest
  | getProductFailed;
