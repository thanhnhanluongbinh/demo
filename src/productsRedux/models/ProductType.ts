export interface productType {
  id: string;
  name: string;
  price: {
    current: {
      text: string;
    };
  };

  imageUrl: string;
}
