import { combineReducers, createStore, compose, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import * as reducers from "./reducers";
import reduxThunk from "redux-thunk";
import { DetailType } from "../models/DetailType";
const rootReducers = combineReducers({
  ...reducers,
});
export type AppState = ReturnType<typeof rootReducers>;
export type AppDispatch = typeof store.dispatch;
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// redux thường

// export const store = createStore(rootReducers, composeEnhancers());

// Redux-thunk

export const store = createStore(
  rootReducers,
  composeWithDevTools(applyMiddleware(reduxThunk))
);
