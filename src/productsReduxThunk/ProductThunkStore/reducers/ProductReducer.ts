import { DetailActionType } from "./../../models/ActionsTypes";

import { ProductActionType, ProductResult } from "../../models/ActionsTypes";
import {
  GET_DETAIL_SUCCESS,
  GET_PRODUCT_FAILED,
  GET_PRODUCT_REQUEST,
  GET_PRODUCT_SUCCESS,
  GET_DETAIL_REQUEST,
  GET_DETAIL_FAILED,
} from "../constants/constants";

const initialState: ProductResult = {
  loading: false,
  productList: [],
  error: "",
  detail: null,
};

export const productReducer = (
  state: ProductResult = initialState,
  action: ProductActionType | DetailActionType
) => {
  switch (action.type) {
    case GET_PRODUCT_REQUEST:
      return { loading: true, productList: [], error: "" };
    case GET_PRODUCT_SUCCESS:
      return { loading: false, productList: action.productList, error: "" };
    case GET_PRODUCT_FAILED:
      return { loading: false, productList: [], error: action.error };

    // detail
    case GET_DETAIL_REQUEST:
      return { ...state, loading: true, detail: null };
    case GET_DETAIL_SUCCESS:
      return { ...state, loading: false, detail: action.detail };
    case GET_DETAIL_FAILED:
      return { ...state, loading: false, detail: null, error: action.error };

    default:
      return state;
  }
};
