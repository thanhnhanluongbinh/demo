import { getProductDetailAPI } from "./../../services/ProductServices";
import {
  GET_DETAIL_REQUEST,
  GET_DETAIL_SUCCESS,
} from "./../constants/constants";
import { DetailActionType } from "./../../models/ActionsTypes";
import { Dispatch } from "redux";
import { ProductActionType } from "../../models/ActionsTypes";
import { DetailType } from "../../models/DetailType";
import { getProductListAPI } from "../../services/ProductServices";
import {
  GET_PRODUCT_REQUEST,
  GET_PRODUCT_SUCCESS,
} from "../constants/constants";
export const getProductList = () => {
  return async (dispatch: Dispatch<ProductActionType>) => {
    try {
      dispatch({
        type: GET_PRODUCT_REQUEST,
        loading: true,
        productList: [],
        error: "",
        detail: null,
      });
      const data = await getProductListAPI();
      console.log(typeof data);
      dispatch({
        type: GET_PRODUCT_SUCCESS,
        loading: false,
        productList: data,
        error: "",
        detail: null,
      });
      console.log("data", data);
    } catch (error) {}
  };
};
export type detailAction = ReturnType<typeof getProductDetail>;
export const getProductDetail = (id: string | undefined) => {
  console.log("id action", id);
  return async (dispatch: Dispatch<DetailActionType>) => {
    try {
      dispatch({
        type: GET_DETAIL_REQUEST,
        loading: true,
        detail: null,
        error: "",
      });
      const data = await getProductDetailAPI(id);
      console.log("data", data);
      dispatch({
        type: GET_DETAIL_SUCCESS,
        loading: false,
        detail: data,
        error: "",
      });
    } catch (error) {}
  };
};
