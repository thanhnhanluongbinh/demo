import axios from "axios";
import React, { Component } from "react";
import { DetailType } from "../models/DetailType";
import { productType } from "../models/ProductType";

export const getProductListAPI = (): Promise<productType[]> => {
  return new Promise<productType[]>((resolve, reject) => {
    const options = {
      method: "GET",
      url: "https://asos2.p.rapidapi.com/products/v2/list",
      params: {
        store: "US",
        offset: "0",
        categoryId: "4209",
        limit: "48",
        country: "US",
        sort: "freshness",
        currency: "USD",
        sizeSchema: "US",
        lang: "en-US",
      },
      headers: {
        "X-RapidAPI-Key": "68e513472amsh48e987d8c1f1417p1b497ejsnf3f5adbfe802",
        "X-RapidAPI-Host": "asos2.p.rapidapi.com",
      },
    };
    axios(options)
      .then((response) => {
        if (response.data) {
          resolve(response.data.products);
        } else {
          reject(response.data.status);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getProductDetailAPI = (
  id: string | undefined
): Promise<DetailType> => {
  return new Promise<DetailType>((resolve, reject) => {
    const options = {
      method: "GET",
      url: "https://asos2.p.rapidapi.com/products/detail",
      params: {
        id: id,
        lang: "en-US",
        store: "US",
        currency: "USD",
        sizeSchema: "US",
      },
      headers: {
        "X-RapidAPI-Key": "68e513472amsh48e987d8c1f1417p1b497ejsnf3f5adbfe802",
        "X-RapidAPI-Host": "asos2.p.rapidapi.com",
      },
    };
    axios(options)
      .then((res) => {
        if (res.data) {
          console.log("data", res.data);
          resolve(res.data);
        } else {
          console.log("reject else");
          reject(res.data);
        }
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
};
