export interface images {
  url: string;
}
export interface DetailType {
  id: string;
  name: string;
  description: string;
  brand: {
    name: string;
  };
  media: {
    images: images[];
  };
  price: {
    current: {
      value: number;
      text: string;
    };
  };
}
