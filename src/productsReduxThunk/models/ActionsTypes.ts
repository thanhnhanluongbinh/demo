import {
  GET_DETAIL_SUCCESS,
  GET_DETAIL_REQUEST,
  GET_DETAIL_FAILED,
} from "./../ProductThunkStore/constants/constants";
import {
  GET_PRODUCT_FAILED,
  GET_PRODUCT_REQUEST,
  GET_PRODUCT_SUCCESS,
} from "../ProductThunkStore/constants/constants";
import { productType } from "./ProductType";
import { DetailType } from "./DetailType";

export interface ProductResult {
  loading: boolean;
  productList: productType[];
  detail: DetailType | null;
  error: string;
}

interface getProductSuccess extends ProductResult {
  type: typeof GET_PRODUCT_SUCCESS;
}
interface getProductFailed extends ProductResult {
  type: typeof GET_PRODUCT_FAILED;
}
interface getProductRequest extends ProductResult {
  type: typeof GET_PRODUCT_REQUEST;
}
export interface DetailResult {
  loading: boolean;
  detail: DetailType | null;
  error: string;
}

interface getDetailSuccess extends DetailResult {
  type: typeof GET_DETAIL_SUCCESS;
}
interface getDetailRequest extends DetailResult {
  type: typeof GET_DETAIL_REQUEST;
}
interface getDetailFailed extends DetailResult {
  type: typeof GET_DETAIL_FAILED;
}

export type ProductActionType =
  | getProductSuccess
  | getProductRequest
  | getProductFailed;
export type DetailActionType =
  | getDetailSuccess
  | getDetailRequest
  | getDetailFailed;
