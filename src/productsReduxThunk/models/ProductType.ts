export interface productType {
  id: string;
  name: string;
  price: {
    current: {
      value: number;
      text: string;
      currency: string;
    };
  };
  color: string;
  productCode: string;
  brand: string;
  imageUrl: string;
}
